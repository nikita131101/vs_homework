﻿#include <iostream>
#include <string>
#include <time.h>
#include <Windows.h>
#include <stack>
#include "Helpers.h"
using namespace std;

class Vector
{
private:
    float x, y, z;
public:
    Vector() : x(1), y(1), z(1)
    {}
    void GetVector()
    {
        cout << x << " " << y << " " << z;
    }
    void GetLength()
    {
        float l = sqrt(x * x + y * y + z * z);
        cout << "\n" << "Длина вектора равна: " << l;
    }
};
class Stack
{
private:
    int i;
    int m_size = 0;
    int* steck = NULL;
public:
    Stack(int size)
    {
        m_size = size;
        steck = new int[m_size];
        for (i = 0; i < m_size; i++)
        {
            steck[i] = i;
        }
        cout << "Ваш стек имеет вид: ";
        for (i = 0; i < m_size; i++)
        {
            cout << steck[i] << "  ";
        }
        cout << "\n";
    }
    void pushElement(int size)
    {
        m_size = size + 1;
        delete steck;
        steck = new int[m_size];
        cout << "Ваш стек имеет вид: ";
        for (i = 0; i < m_size; i++)
        {
            steck[i] = i;
            cout << steck[i] << "  ";
        }
        cout << "\n";
    }
    void popElement(int size)
    {
        m_size = size - 1;
        delete steck;
        steck = new int[m_size];
        cout << "Ваш стек имеет вид: ";
        for (i = 0; i < m_size; i++)
        {
            steck[i] = i;
            cout << steck[i] << "  ";
        }
        cout << "\n";
    }
};

void function_1(int N, int parity)
{
    if ((parity == 0) || (parity == 1))
    {
        cout << "Ваши числа:\n";
        for (int i = 0; i <= N; i++)
        {
            if (i % 2 == parity)
            {
                cout << i << "\n";
            }
        }
    }
    else cout << "Ошибка в печати!";
}

int main()
{
    setlocale(LC_ALL, "Russian");
    int n;
    cout << "Введите номер домашней работы: ";
    cin >> n;
    switch (n)
    {
    case 2:
    {
        int a, b;
        cout << "\n" << "ЗАДАНИЕ 2\n" << "Введите a: ";
        cin >> a;
        cout << "Введите b: ";
        cin >> b;
        int result = func(a, b);
        cout << "Результат: " << result << endl;
        break;
    }
    case 3:
    {
        string name;
        cout << "\n" << "ЗАДАНИЕ 3\n" << "Введите своё имя без пробелов и на английском: ";
        cin >> name;
        cout << "Длина вашего имени: " << name.size() << "\n";
        int k = name.size();
        cout << "Первые и последние буквы имени соответственно: " << name[0] << " и ";
        for (int i = 0; i <= k; i++)
        {
            if (i == (k - 1))
            {
                cout << name[i];
            }
        }
        break;
    }
    case 4:
    {
        int parity, N;
        cout << "\n" << "ЗАДАНИЕ 4\n" << "Введите число, до которого будет осуществляться вывод чисел: ";
        cin >> N;
        cout << "Вывести чётные (0) или нечётные (1) числа от 0 до " << N << "? Ваш ответ в виде цифры: ";
        cin >> parity;
        function_1(N, parity);
        break;
    }
    case 5:
    {
        const int N = 6;
        int array[N][N];
        int k, S = 0;
        cout << "\n" << "ЗАДАНИЕ 5\n" << "Массив:\n";
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                array[i][j] = i + j;
                cout << array[i][j] << "  ";
            }
            cout << "\n";
        }
        SYSTEMTIME st;
        GetLocalTime(&st);
        cout << "Текущее число календаря: " << st.wDay << "\n";
        k = st.wDay % N;
        for (int i = 0; i < N; i++)
        {
            if (i == k)
            {
                for (int j = 0; j < N; j++)
                {
                    S = S + array[i][j];
                }
            }
        }
        cout << "Сумма элементов строки под номером " << k + 1 << " равна: " << S;
        break;
    }
    case 6:
    {
        cout << "\n" << "ЗАДАНИЕ 6\n" << "Дан вектор с координатами: ";
        Vector coordinate, length;
        coordinate.GetVector();
        length.GetLength();
        break;
    }
    case 7:
    {
        int size;
        bool k, l;
        cout << "\n" << "ЗАДАНИЕ 7\n" << "Введите стартовый объём стека: ";
        cin >> size;
        Stack memory(size);
        cout << "Хотите внести изменения в стек: да - 0, нет - 1? Ваш ответ: ";
        cin >> k;
        while (k != true)
        {
            cout << "Хотите добавить (0) или убрать (1) один элемент стека? Ваш ответ: ";
            cin >> l;
            if (l == false)
            {
                size = size + 1;
                memory.pushElement(size);
            }
            else
            {
                size = size - 1;
                memory.popElement(size);
            }
            cout << "Хотите внести изменения в стек: да - 0, нет - 1? Ваш ответ: ";
            cin >> k;
        }
        break;
    }
    default:
        cout << "\n" << "Такой работы нет!";
    }
}
